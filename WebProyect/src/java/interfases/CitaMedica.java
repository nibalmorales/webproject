package interfases;

public interface CitaMedica {

    public void imprimirCita();

    public void imprimirConstancia(String descripcion);

}

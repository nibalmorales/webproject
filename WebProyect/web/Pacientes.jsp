
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <title>Registro de Vacunación</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- CSS only -->
        <link rel="stylesheet" type="text/css" href="bootstrap.min.css">
    </head>

    <body>
        <nav class="navbar navbar-light bg-primary">
            <a class="navbar-brand" href="#" style="color:white">Registro de Datos</a>
        </nav>
        <div class="container">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">Vacuna Covid-19</h1>
                    <p class="lead">Cuando sea tu momento #Vacúnate</p>
                </div>
            </div>    
            <h2>Datos Generales</h2>
            <br> 
            <form>  
                <div class="container" >
                    
                    
                    <form action="response.jsp" style="margin:5%; background-color:#F2F2F2; padding: 5%; ">
                        <!-- primera fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-6">     
                                    <label for="lblNacionalidad" class="form-label">Nacionalidad *</label>
                                    <select class="form-control" >
                                        <option>guatemalteco</option>
                                    </select>
                                    
                                    <%
                                    
                                    
                                    %>
                                </div>
                                <!-- segunda columna -->
                                <!-- <div class="col-md-4">
                                     <input  class="form-control" type="text" name="nombres" placeholder="Nombres">
                                 </div>-->
                                <!--tercera columna -->
                                <div class="col-md-6">
                                    <label for="lblCUI" class="form-label">C.U.I.*</label>
                                    <input  class="form-control" type="text" name="InputCUI" placeholder="2054826960201">
                                </div>
                            </div>
                        </div>


                        <!-- segunda fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-4">
                                    <label for="lblPrimerNombre" class="form-label">Nombres *</label>
                                    <input  class="form-control" type="text" name="InputPrimerNombre" placeholder="Silvestra">
                                </div>
                                <!-- segunda columna -->
                                <div class="col-md-4">
                                    <label for="lblSegundoNombre" class="form-label">&nbsp;</label>
                                    <input  class="form-control" type="text" name="InputSegundoNombre" placeholder="Aurora">
                                </div>
                                <!-- tercera columna -->
                                <div class="col-md-4">	
                                    <label for="lblTercerNombre" class="form-label"> &nbsp;</label>
                                    <input  class="form-control" type="text" name="InputTercerNombre" placeholder="Tercer Nombre">
                                </div>
                            </div>
                        </div>


                        <!-- tercera fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-6">     
                                    <label for="lblPrimerApellido" class="form-label">Apellidos *</label>
                                    <input  class="form-control" type="text" name="InutPrimerApellido" placeholder="Alvarado">
                                </div>
                                <!-- segunda columna -->
                                <!-- <div class="col-md-4">
                                     <input  class="form-control" type="text" name="nombres" placeholder="Nombres">
                                 </div>-->
                                <!--tercera columna -->
                                <div class="col-md-6">
                                    <label for="lblSegundoApellido" class="form-label">&nbsp;</label>
                                    <input  class="form-control" type="text" name="InputSegundoApellido" placeholder="De León">
                                </div>
                            </div>
                        </div>


                        <!-- cuarta fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-4">
                                    <label for="lblSexo" class="form-label">Sexo *</label>
                                    <select class="form-control" >
                                        <option>Mujer</option>
                                        <option>Hombre</option>
                                    </select>
                                </div>
                                <!-- segunda columna -->
                                <div class="col-md-4">
                                    <label for="lblFechaNacimiento" class="form-label">Fecha de Ncimiento</label>
                                    <input  class="form-control" type="text" name="InputFechaNacimiento" placeholder="Aurora">
                                </div>
                                <!-- tercera columna -->
                                <div class="col-md-4">	
                                    <label for="lblEscolaridad" class="form-label">Escolaridad</label>
                                    <select class="form-control" >
                                        <option>Pre-Primaria</option>
                                        <option>Primaria</option>
                                        <option>Básicos</option>
                                        <option>Diversificado</option>
                                        <option>Universidad</option>
                                        <option>No Aplica</option>
                                        <option>No Indica</option>
                                        <option>Otro</option>
                                        <option>Ninguno</option>
                                    </select>
                                </div>
                            </div>
                        </div>



                        <!-- Quinta fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-6">     
                                    <label for="lblDptoResidencia" class="form-label">Departamento Residencia *</label>
                                    <input  class="form-control" type="text" name="InputDptoResidencia" placeholder="Alvarado">
                                </div>
                                <!-- segunda columna -->
                                <!-- <div class="col-md-4">
                                     <input  class="form-control" type="text" name="nombres" placeholder="Nombres">
                                 </div>-->
                                <!--tercera columna -->
                                <div class="col-md-6">
                                    <label for="lblMuniResidencia" class="form-label">Municipio Residencia *</label>
                                    <input  class="form-control" type="text" name="InputMuniResidencia" placeholder="De León">
                                </div>
                            </div>
                        </div>


                        
                        <!-- sexta fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-12">     
                                    <label for="lblDireccionResidencia" class="form-label">Dirección de Residencia *</label>
                                    <input  class="form-control" type="text" name="InputDireResidencia" placeholder="Alvarado">
                                </div>
                                <!-- segunda columna -->
                                <!-- <div class="col-md-4">
                                     <input  class="form-control" type="text" name="nombres" placeholder="Nombres">
                                 </div>-->
                                <!--tercera columna 
                                <div class="col-md-6">
                                     <label for="exampleFormControlInput1" class="form-label">Municipio Residencia*</label>
                                    <input  class="form-control" type="text" name="rh" placeholder="De León">
                                </div>-->
                            </div>
                        </div>



                        <!-- septima fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                           <div class="col-md-6">
                                    <label for="lblPueblo" class="form-label">Pueblo *</label>
                                    <select class="form-control">
                                        <option>Garifuna</option>
                                        <option>Ladino/Mestizo</option>
                                        <option>Maya</option>
                                        <option>Xinca</option>    
                                    </select>
                                </div>
                                <!-- segunda columna -->
                                <!-- <div class="col-md-4">
                                     <input  class="form-control" type="text" name="nombres" placeholder="Nombres">
                                 </div>-->
                                <!--tercera columna -->
                                <div class="col-md-6">
                                    <label for="lblComunidadLing" class="form-label">Comunidad Linguistica *</label>
                                    <select class="form-control">
                                        <option>Español/Castellano</option>
                                        <option>Achi</option>
                                        <option>Akateko</option>
                                        <option>Chorti</option>
                                        <option>Chuj</option>
                                        <option>Itza</option>
                                        <option>Ixil</option>
                                        <option>Jakalteko</option>
                                        <option>Qanjobal</option>
                                        <option>Kaqchikel</option>                                       
                                        <option>Kiche</option>
                                        <option>Man</option>
                                        <option>Mopan</option>
                                        <option>Poqomam</option>                                        
                                        <option>Poqomchi</option>                                       
                                        <option>Qeqchi</option>
                                        <option>Sakapulteko</option>
                                        <option>Sipakapense</option>
                                        <option>Tektiteko</option>
                                        <option>Tzutujil</option>
                                        <option>Uspanteko</option>
                                        <option>Xinca</option>
                                        <option>Garifuna</option>     
                                    </select>
                                </div>
                            </div>
                        </div>


                        <!-- octava fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-6">     
                                    <label for="lblCorreo" class="form-label">Correo Electónico</label>
                                    <input  class="form-control" type="text" name="InputCorreo" placeholder="Correo Electrónico">
                                </div>
                                <!-- segunda columna -->
                                <div class="col-md-4">
                                    <label for="lblcelular" class="form-label">Teléfono Celular</label>
                                    <input  class="form-control" type="text" name="InputCelular" placeholder="Teléfono Celular">
                                </div>
                                <!--tercera columna -->
                                <div class="col-md-2">
                                    <label for="lblLinea" class="form-label">Linea</label>
                                        <select class="form-control" >
                                        <option>Claro</option>
                                        <option>Tigo</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        
                        <!-- octava fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-6">     
                                </div>
                                <!--segunda columna -->
                                <div class="col-md-6">
                                    <label for="lblTelefono" class="form-label">Confirmar Teléfono</label>
                                    <input  class="form-control" type="text" name="InputTelefono" placeholder="Repetir Teléfono Célular">
                                </div>
                            </div>
                        </div>


                        <!-- novena fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-6">     
                                    <label for="lblDiscapacidad" class="form-label">Discapacidad *</label>
                                    <select class="form-control" >
                                        <option>Auditiva</option>
                                        <option>Fisica</option>
                                        <option>Mental</option>
                                        <option>Multiple</option>
                                        <option>No Aplica</option>
                                        <option>Otro</option>
                                        <option>Visual</option> 
                                    </select>
                                </div>
                                <!-- segunda columna -->
                                <!-- <div class="col-md-4">
                                     <input  class="form-control" type="text" name="nombres" placeholder="Nombres">
                                 </div>-->
                                <!--tercera columna -->
                                <div class="col-md-6">
                                    <label for="lblEnfermedadCronica" class="form-label">Enfermedad Crónica *</label>
                                    <select class="form-control" >
                                        <option>Diabet mellitus</option>
                                        <option>Enfermedad pulmonary cronica</option>
                                        <option>Enfermedad renal croica</option>
                                        <option>Enfermedades cariovasculares</option>
                                        <option>Enfermedades cerebrobasculares</option>
                                        <option>Hipertension arterial</option>
                                        <option>Inmunosupresion arterial</option>                          
                                        <option>Inmunosupresion-uso-inmunosupresores</option> 
                                        <option>Inmunosupresion-VIH</option> 
                                        <option>Ninguno</option> 
                                        <option>Obesidad</option> 
                                        <option>otra</option> 
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br> <br>
                        
                        
                        
                        <!-- decima fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-6">     
                                    <label for="lblAfiliacionIgss" class="form-label">Tiene afiliación de IGSS?</label>
                                    <br><br>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadio1">Si</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadio2">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>




                        <br>
                        <h2>Puesto de Vacunación</h2>
                        <br><br>


                        <!-- decima primera fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-6">     
                                    <label for="lblDpto" class="form-label">Departamento *</label>
                                    <input  class="form-control" type="text" name="InputDpto" placeholder="">
                                </div>
                                <!-- segunda columna -->
                                <!-- <div class="col-md-4">
                                     <input  class="form-control" type="text" name="nombres" placeholder="Nombres">
                                 </div>-->
                                <!--tercera columna -->
                                <div class="col-md-6">
                                    <label for="lblMunicipio" class="form-label">Municipio *</label>
                                    <input  class="form-control" type="text" name="InputMunicipio" placeholder="">
                                </div>
                            </div>
                        </div>


                        <!-- decima segunda fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-12">     
                                    <label for="lblPuestoVacunacion" class="form-label">Puesto de Vacunación *</label>
                                    <input  class="form-control" type="text" name="InputPuestoVacunacion" placeholder="">
                                </div>
                            </div>
                        </div>


                        <br><br>
                        <!-- Boton Registrat -->
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">     
                                    <button type="button"  class="btn btn-primary btn-lg btn-block" value="Prueba Alert" onclick="EventoAlert()">Ingresar Datos</button>
                                    <script>
                                        function EventoAlert() {

                                        }
                                    </script>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
        </div>
    </body>
</html>

